/**
 * Created by yura on 15.11.15.
 */
var Main = {
    init:function(){

        if (typeof columns !== 'undefined'){
            $.extend( $.fn.dataTable.defaults, {

                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',

                paging: true,


                drawCallback: function () {

                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');

                },
                preDrawCallback: function() {

                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                },

            });


            $('.datatable-sorting').dataTable( {
                "ajax": {
                    "url": "",
                    "type": "POST",
                    "succes" : function (data) {
                        console.log(data);
                    },
                    "error" :function (data) {
                        console.log('error');
                        console.log(data);
                    }
                },
                "serverSide": true,
                "processing": true,
                "paging": true,
                "columns": columns

            });




            $('.dataTables_filter input[type=search]').attr('placeholder','Type to filter...');

            $('.dataTables_length').find('select').select2({
                minimumResultsForSearch: "-1"
            });

        }


        $('.open-popup-link').on('click',function () {
            $.ajax({
                url: "/preview",
                data: $('.open-popup-link').closest('form').serialize(),
                method: 'post',
                dataType: 'html',
                cache: false
            })
                .done(function( html ) {
                    $( "#modal" ).html( html );

                    $.magnificPopup.open({
                        type:'inline',
                        items: {
                            src: '#modal',
                            type: 'inline'
                        }
                    });
                });


        });

        if ($('#image').val() != ''){


            var options = {
                'allowedFileExtensions' : ['jpg','png','gif'],
                'maxFileSize': 5120,
                'maxFileCount': 1,
                'uploadUrl': '/image',
                'showUpload': false,
                'elErrorContainer': '#image-error',
                'uploadAsync': true,
                'initialPreviewFileType': 'image',
                'initialPreview': [
                    "<img src='"+$('#image').val()+"' class='file-preview-image' style='max-width: 165px' alt='Jelly Fish'>"
                ],
                'msgInvalidFileExtension': 'Invalid extension for file "{name}". Only "{extensions}" files are supported.'

            };
        }else{
            var options = {
                'allowedFileExtensions' : ['jpg','png','gif'],
                'maxFileSize': 5120,
                'maxFileCount': 1,
                'uploadUrl': '/image',
                'showUpload': false,
                'elErrorContainer': '#image-error',
                'uploadAsync': true,
                'msgInvalidFileExtension': 'Invalid extension for file "{name}". Only "{extensions}" files are supported.'

            };
        }


        $("#file-upload").fileinput(options);

        $("#file-upload").on('fileloaded',function(event, file, previewId, index, reader){
            $("#file-upload").fileinput('upload');
            $('#image').val('/img/common/'+file['name']);
        });


        $("#file-upload").on('fileclear',function(event, id){

            $('#image').val('');
        });

        $('input[type=text]').on('click',function(){
            var id = this.name;
            var el = $('#'+id+'-error');
            el.html('');

            $(this).closest('.form-group').removeClass('has-error').removeClass('has-feedback');
            $('#'+id+'-error-icon').hide();
        });

        $('input[type=password]').on('click',function(){
            var id = this.name;
            var el = $('#'+id+'-error');
            el.html('');

            $(this).closest('.form-group').removeClass('has-error').removeClass('has-feedback');
            $('#'+id+'-error-icon').hide();
        });

        $('textarea').on('click',function(){
            var id = this.name;
            var el = $('#'+id+'-error');
            el.html('');

            $(this).closest('.form-group').removeClass('has-error').removeClass('has-feedback');
            $('#'+id+'-error-icon').hide();
        });

        $('select').on('click',function(){
            var id = this.name;
            var el = $('#'+id+'-error');
            el.html('');

            $(this).closest('.form-group').removeClass('has-error').removeClass('has-feedback');
            $('#'+id+'-error-icon').hide();
        });

        $('.form-control-feedback').each(function(){
            $(this).hide();
        });

    },
    formSubmit:function(form){

        $(form).find('button[type=submit]').attr( "disabled", "disabled" );
        var url = form.action;
        $.ajax({
            type: "POST",
            url: url,
            data: $(form).serialize(),
            dataType: "json",
            success: function(data){
                console.log(data);
                Main.actionData(data,form);
            },
            error: function (data) {
                console.log(data);
            }
        });

        return false;
    },
    showNotifyMessage: function(title,text,type){

        if(!type)
            type='success';

        new PNotify({
            title: title,
            text: text,
            type: type
        });
    },
    showValidationMessage: function(data,form){

        $('input[type=text]').each(function () {
            var id = this.name;
            var el = $('#'+id+'-error');
            el.html('');

            $(this).closest('.form-group').removeClass('has-error').removeClass('has-feedback');
            $('#'+id+'-error-icon').hide();
        });

        $('input[type=password]').each(function () {
            var id = this.name;
            var el = $('#'+id+'-error');
            el.html('');

            $(this).closest('.form-group').removeClass('has-error').removeClass('has-feedback');
            $('#'+id+'-error-icon').hide();
        });

        $('textarea').each(function(){
            var id = this.name;
            var el = $('#'+id+'-error');
            el.html('');

            $(this).closest('.form-group').removeClass('has-error').removeClass('has-feedback');
            $('#'+id+'-error-icon').hide();
        });

        $(form).find('button[type=submit]').removeAttr("disabled", "disabled");
        $.each(data,function(i,value){
            var input = $('#'+i);
            var el = $('#'+i+'-error');
            input.closest('.form-group').addClass('has-error').addClass('has-feedback');
            $('#'+i+'-error-icon').show();
            el.html(value);
        });
    },
    actionData: function(data,form){
        if(typeof data['redirect'] != 'undefined'){
            $('.modified').each(function(){
                $(this).removeClass('modified');
            });
            if(data['redirect']=='')
                window.location.reload();
            else
                window.location = data['redirect'];
        }else{
            if(typeof data['message'] != 'undefined'){
                Main.showNotifyMessage(data['title'],data['message'],data['type']);
                $(form).find('button[type=submit]').removeAttr("disabled", "disabled");
            }
            else
                Main.showValidationMessage(data,form);
        }
    },
    getLangLink:  function(uri){
        var lang = $('html').attr('lang');
        if(lang)
            return '/'+lang+uri;
        else
            return uri;
    }

};

$(document).ready(function(){
    Main.init();
});
