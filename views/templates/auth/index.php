<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sign in to application</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <link href="/css/signin.css" rel="stylesheet">
    <link href="/css/notify.css" rel="stylesheet">
</head>

<body>

<div class="container">

    <form class="form-signin" action="/postlogin" method="post" onsubmit="return Main.formSubmit(this);">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="name" class="sr-only">Name</label>
        <input type="text" name="name" id="name" class="form-control" placeholder="name"  autofocus>
        <label for="password" class="sr-only">Password</label>
        <input type="password" name="password" id="password" class="form-control" placeholder="Password">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>

</div>
<script src="/js/jquery.min.js"></script>
<script src="/js/main.js"></script>
<script src="/js/pnotify.min.js"></script>

<?php if (!empty($message)) :?>

    <script>
        $(document).ready(function () {
            Main.showNotifyMessage('<?php echo @$message['title']?>','<?php echo @$message['message']?>','<?php echo @$message['type']?>');
        });
    </script>

<?php endif?>
</body>
</html>
