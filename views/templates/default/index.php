<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tasks Manager</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/icons/icomoon/styles.css" rel="stylesheet">
    <link href="/css/fileinput.min.css" rel="stylesheet">
    <link href="/css/select2.css" rel="stylesheet">
    <link href="/css/dashboard.css" rel="stylesheet">
    <link href="/css/magnific-popup.css" rel="stylesheet">
    <link href="/css/notify.css" rel="stylesheet">
    <link href="/css/dataTables.css" rel="stylesheet">

</head>

<body>
<nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
    <button class="navbar-toggler navbar-toggler-right hidden-lg-up" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">Task Manager</a>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <?php if (\Core\Auth::getUser()):?>
                    <a class="nav-link" href="/logout">Logout</a>
                <?php else:?>
                    <a class="nav-link" href="/login">Login</a>
                <?php endif;?>
            </li>
        </ul>

        <div class="form-inline mt-2 mt-md-0">
            <a href="/add" class="btn btn-outline-success my-2 my-sm-0">Add new task</a>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">

        <main class="col-md-12 pt-3">
            <div class="container mb-3">
                <h1><?php echo $titleContent?></h1>

                <?php if (isset($mainContent) && $mainContent):?>
                    <?php echo $mainContent;?>
                <?php endif;?>
            </div>

        </main>
    </div>
</div>

<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/select2.min.js"></script>
<script src="/js/datatables.min.js"></script>
<script src="/js/fileinput.min.js"></script>
<script src="/js/jquery.magnific-popup.js"></script>
<script src="/js/main.js"></script>
<script src="/js/pnotify.min.js"></script>

<?php if (!empty($message)) :?>

    <script>
        $(document).ready(function () {
            Main.showNotifyMessage('<?php echo @$message['title']?>','<?php echo @$message['message']?>','<?php echo @$message['type']?>');
        });
    </script>

<?php endif?>

</body>
</html>