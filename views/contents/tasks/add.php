<form method="post" action="<?php echo route()->generate('submit_task')?>" onsubmit="return Main.formSubmit(this);">
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Name *">
        <span class="help-block" id="name-error"></span>
    </div>
    <div class="form-group">
        <label for="email">Email address</label>
        <input type="text" class="form-control" id="email" name="email" placeholder="Email *">
        <span class="help-block" id="email-error"></span>
    </div>

    <div class="form-group">
        <label for="text">Description</label>
        <textarea class="form-control" id="text" name="text" rows="10" placeholder="Description *"></textarea>
        <span class="help-block" id="text-error"></span>
    </div>
    <div class="form-group  col-md-3">
        <label for="exampleInputFile">Image: </label>
        <input type="file" name="file" id="file-upload">
        <span class="help-block" id="image-error"></span>
        <input type="hidden" value="" name="image" id="image">
    </div>
    <div class="form-group">
        <a href="#modal" class="btn btn-outline-danger open-popup-link">Preview</a>
        <button type="submit" class="btn btn-primary">Submit</button>

    </div>

</form>


<div id="modal" class="white-popup mfp-hide">

</div>



