<form method="post" action="/task/update/<?php echo $task['id']?>" onsubmit="return Main.formSubmit(this);">
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Name *" value="<?php echo $task['name']?>">
        <span class="help-block" id="name-error"></span>
    </div>
    <div class="form-group">
        <div class="checkbox">
            <label> <input type="checkbox" id="completed" name="completed" <?php echo ($task['completed'] == 1)? 'checked': ''?> > Complete task </label>
        </div>
        <span class="help-block" id="email-error"></span>
    </div>

    <div class="form-group">
        <label for="email">Email address</label>
        <input type="text" class="form-control" id="email" name="email" placeholder="Email *" value="<?php echo  $task['email']?>">
        <span class="help-block" id="email-error"></span>
    </div>

    <div class="form-group">
        <label for="text">Description</label>
        <textarea class="form-control" id="text" name="text" rows="10" placeholder="Description *"><?php echo $task['text']?></textarea>
        <span class="help-block" id="text-error"></span>
    </div>
    <div class="form-group  col-md-3">
        <label for="exampleInputFile">Image: </label>
        <input type="file" name="file" id="file-upload">
        <span class="help-block" id="image-error"></span>
        <input type="hidden" value="<?php echo $task['image']?>" name="image" id="image">
    </div>
    <div class="form-group">
        <a href="#modal" class="btn btn-outline-danger open-popup-link">Preview</a>
        <button type="submit" class="btn btn-primary">Submit</button>

    </div>

</form>


<div id="modal" class="white-popup mfp-hide">

</div>



