<table id="table-main" class="table   datatable-sorting ">
    <thead>
    <tr>
        <?php foreach($data['header'] as $name=>$column):?>
            <th><?php echo $name?></th>
        <?php endforeach;?>

    </tr>
    </thead>

    <tbody>

    </tbody>


</table>

<script>

    var columns = [
            <?php foreach ($data['header'] as $name=>$column):?>
            {
                name: "<?php echo $name?>",
                data: "<?php echo $name?>" ,
                <?php if(!empty($column['width'])):?> "width": "<?php echo $column['width']?>", <?php endif;?>
                <?php if(!empty($column['className'])) :?>"className": "<?php echo $column['className']?>", <?php endif;?>
                "orderable": <?php echo ($column['order']) ? 'true' : 'false' ?>,
                "searchable": <?php echo (!empty($column['searchable']) && $column['searchable'])?'true':'false' ?>
            },
            <?php endforeach;?>
        ];

</script>