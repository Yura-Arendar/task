<?php

namespace Core;

class Request
{

    /**
     * check for ajax request
     * @return bool
     */
    public static function isAjax()
    {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
    }

    /**
     * return $_GET param
     * @param $key
     * @return null|string
     */
    public static function _GET($key)
    {
        return isset($_GET[$key]) ? trim($_GET[$key]) : null;
    }

    /**
     * return $_POST param
     * @param $key
     * @return null|string
     */
    public static function _POST($key)
    {
        return isset($_POST[$key]) ? trim($_POST[$key]) : null;
    }

    /**
     * check if this post request
     * @return bool
     */
    public static function is_post()
    {
        return $_SERVER['REQUEST_METHOD'] == 'POST';
    }

    /**
     * get current request method
     * @return string
     */
    public static function get_method()
    {
        $method =  $_SERVER['REQUEST_METHOD'];

        if(self::is_post()){
            if(isset($_SERVER['X-HTTP-METHOD-OVERRIDE'])){
                $method = strtoupper($_SERVER['X-HTTP-METHOD-OVERRIDE']);
            }
        }

        return $method;
    }

    /**
     * check for https protocol
     * @return bool
     */
    public static function is_https()
    {
        return isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off';
    }

    /**
     * get host with protocol
     * @return string
     */
    public static function get_http_host()
    {
        $host = self::is_https() ? 'https://' : 'http://';
        $host .= Request::get_host();
        return $host;
    }

    /**
     * get current host
     * @return string
     */
    public static function get_host()
    {
        $host = $_SERVER['HTTP_HOST'];

        $host = strtolower(preg_replace('/:\d+$/', '', trim($host)));

        if ($host && !preg_match('/^\[?(?:[a-zA-Z0-9-:\]_]+\.?)+$/', $host)) {
            throw new \UnexpectedValueException(sprintf('Invalid Host "%s"', $host));
        }

        return $host;
    }


    /**
     * all data from post
     * @return array
     */
    public static function postAll()
    {
        $array = [];

        foreach ($_POST as $key=>$value)
        {
            $array[$key] = Request::_POST($key);
        }

        return $array;
    }

    /**
     * all data from get
     * @return array
     */
    public static function getAll()
    {
        $array = [];

        foreach ($_GET as $key=>$value)
        {
            $array[$key] = Request::_GET($key);
        }

        return $array;
    }

    /**
     * return url  path
     * @param null $baseUrl
     * @return string
     */
    public static function get_path_info($baseUrl = null)
    {
        static $pathInfo;

        if (!$pathInfo) {
            $pathInfo = $_SERVER['REQUEST_URI'];

            if (!$pathInfo) {
                $pathInfo = '/';
            }

            $schemeAndHttpHost = self::is_https() ? 'https://' : 'http://';
            $schemeAndHttpHost .= $_SERVER['HTTP_HOST'];

            if (strpos($pathInfo, $schemeAndHttpHost) === 0) {
                $pathInfo = substr($pathInfo, strlen($schemeAndHttpHost));
            }

            if ($pos = strpos($pathInfo, '?')) {
                $pathInfo = substr($pathInfo, 0, $pos);
            }

            if (null != $baseUrl) {
                $pathInfo = substr($pathInfo, strlen($pathInfo));
            }

            if (!$pathInfo) {
                $pathInfo = '/';
            }
        }

        return $pathInfo;
    }

}