<?php
namespace Core;

class View
{
    /**
     * object of View class
     * @var View
     */
    private static $instance;

    /**
     * global variables
     * @var array
     */
    private $global_var = [];

    /**
     * View constructor.
     */
    private function __construct(){}

    /**
     * @return View
     */
    public static function instance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new View();
        }

        return static::$instance;
    }

    /**
     * Add new global variable
     * @param $name
     * @param $value
     */
    public function share($name,$value)
    {
        $this->global_var[$name] = $value;
    }

    /**
     * render a  view file
     * @param $file
     * @param array $variables
     * @return string
     */
    public function render($file, $variables = [])
    {
        extract($variables, EXTR_SKIP);

        if ($this->global_var)
        {
            extract($this->global_var, EXTR_SKIP | EXTR_REFS);
        }

        ob_start();

        include path('/views/'.$file.config('app.view_extension'));

        return ob_get_clean();
    }

    /**
     * return a global variables
     * @return array
     */
    public function getGlobal()
    {
        return $this->global_var;
    }
}