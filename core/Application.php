<?php
namespace Core;

use App\Controllers\BaseController;
use App\Controllers\ErrorsController;
use App\Helpers\Log;

class Application
{

    /**
     * instance of applications
     * @var Application
     */
    private static $app;

    /**
     * object of current controller
     * @var BaseController
     */
    private $controller;

    /**
     * name of controller action
     * @var string
     */
    private $action;

    /**
     * current route parameters
     * @var array
     */
    private $parameters = [];

    /**
     * init application
     * Application constructor.
     */
    private function __construct()
    {
        $route = route()->match(Request::get_method(),Request::get_path_info());

        if ($route === null){
            $this->controller = new ErrorsController();
            $this->action = 'error404';
        }else{
            $controller = $route->getController();
            $this->controller = new $controller;
            $this->action = $route->getAction();
            $this->parameters = $route->getParameters();
        }

    }

    /**
     * show 404 error
     */
    public function error404()
    {
        $this->controller = new ErrorsController();
        $this->action = 'error404';

        exit($this->body());
    }

    /**
     * return instance of application
     * @return Application
     */
    public static function instance()
    {
        if (is_null(static::$app)) {
            static::$app = new static;
        }

        return static::$app;
    }

    /**
     * display data for application
     * @return mixed|string
     */
    public function body()
    {

        $content =  call_user_func_array(array($this->controller, $this->action), $this->parameters);

        if (Request::isAjax()) return $content;

        if ($this->controller->render){
            extract(['mainContent'=>$content]);

            if (!empty($this->controller->message))
                extract(['message'=>$this->controller->message]);

            if (!empty($this->controller->titleContent))
                extract(['titleContent'=>$this->controller->titleContent]);

            if (!empty(view()->getGlobal()))
                extract(view()->getGlobal());

            ob_start();

            include path('/views/templates/'.$this->controller->template.'/index'.config('app.view_extension'));

            return ob_get_clean();
        }else{
            return $content;
        }

    }

    /**
     * redirect user to $url with $message
     * @param $url
     * @param $message
     */
    public function redirect($url,$message)
    {
        $_SESSION['message'] = $message;

        if (Request::isAjax()){
            $this->getJson(array('redirect'=>$url));
        }else{
            header('Location: '.$url);
            exit;
        }
    }

    /**
     * return json string from application
     * @param $data
     */
    public function getJson($data)
    {
        exit(json_encode($data));
    }

}