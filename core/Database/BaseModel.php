<?php

namespace Core\Database;

abstract class BaseModel extends Query
{
    /**
     * object of DB class for execute queries
     * @var DB
     */
    private $DB;

    /**
     * BaseModel constructor.
     */
    public function __construct()
    {
        $this->DB = new DB();
    }

    /**
     * update data records with id = $id
     * return id of record
     * @param $id
     * @param array $values
     * @return int
     */
    public function update($id,$values = [])
    {
        $this->where('id',$id);

        $query = $this->getQueryUpdate($values);

        $this->DB->query($query,array_merge($this->updateArguments,$this->arguments));

        return $id;
    }

    /**
     * insert data to current table
     * return id of inserting record
     * @param array $values
     * @return string
     */
    public function insert(array $values)
    {
        $query = $this->getQueryInsert($values);

        $this->DB->query($query,$this->arguments);

        return $this->DB->lastInsertId();
    }


    /**
     * get all records with current filter params
     * @param array $columns
     * @return array
     */
    public function get($columns = [])
    {
        if (!empty($columns)) $this->select = $columns;

        $args = $this->arguments;

        return $this->DB->select($this->getQuerySelect(),$args);
    }

    /**
     * return array of first records in query
     * @param array $columns
     * @return null|array
     */
    public function first($columns = [])
    {
        if (!empty($columns)) $this->select = $columns;

        $args = $this->arguments;

        $values = $this->DB->select($this->getQuerySelect(),$args);

        return empty($values) ? null : $values [0];
    }

    /**
     * return value of columns in current record
     * @param $nameCell
     * @return array|null
     */
    public function cell($nameCell)
    {
        $row = $this->first([$nameCell]);

        return is_null($row) ? $row : $row [$nameCell];
    }

    /**
     * return array for record with id = $id
     * @param $id
     * @param array $columns
     * @return null|array
     */
    public function find($id,$columns = [])
    {
        $this->where('id',$id);

        if (!empty($columns)) $this->select = $columns;

        $args = $this->arguments;

        $values = $this->DB->select($this->getQuerySelect(),$args);

        return empty($values) ? null : $values [0];
    }


}