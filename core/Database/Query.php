<?php
namespace Core\Database;

use App\Helpers\Log;

class Query implements QueryInterface
{

    /**
     * current query string
     * @var string
     */
    protected $query;

    /**
     * current table
     * @var string
     */
    protected $table;

    /**
     * array of current WHERE params
     * @var array
     */
    protected $where = [];

    /**
     * limit params  for current query
     * @var string
     */
    protected $limit;

    /**
     * order by params for current query
     * @var string
     */
    protected $orderBy ;

    /**
     * list of fields in select query
     * @var array
     */
    protected $select = [];

    /**
     * array of arguments for current query
     * @var array
     */
    protected $arguments = [];

    /**
     * arguments for update query
     * @var array
     */
    protected $updateArguments = [];

    /**
     * boolean sql operator or|and
     * @var string
     */
    private $connector = 'and';

    /**
     * fields for inserting or updating in current table
     * @var array
     */
    protected $fillable = [];

    /**
     * generate array for WHERE query parameters
     * @param $column
     * @param null $operator
     * @param null $value
     * @return $this
     */
    public function where($column, $operator = null, $value = null)
    {
        if (is_array($column)){
            foreach ($column as $item)
            {
                $array [] = empty($this->where) ? '' : $this->connector;
                $array [] = $item[0];
                $array [] = (count($item) == 2) ? '=' : $item[1];
                $this->arguments [] = (count($item) == 2) ?  $item[2] : $item[3];
                $array [] = '?' ;

                $this->where [] = $array;
            }
        }else{
            $numargs = func_num_args();

            if ($numargs == 2){
                $this->where[] = [
                    empty($this->where) ? '' : $this->connector,
                    $column,
                    '=',
                    '?'
                ];
                $this->arguments [] = $operator;
            }

            else{
                $this->where[] = [
                    empty($this->where) ? '' : $this->connector,
                    $column,
                    $operator,
                    '?'
                ];
                $this->arguments [] = $value;
            }

        }

        $this->connector = 'and';

        return $this;
    }

    /**
     * generate array for WHERE query parameters with OR boolean operator
     * @param $column
     * @param null $operator
     * @param null $value
     * @return $this
     */
    public function orWhere($column, $operator = null, $value = null)
    {
        $this->connector = 'or';

        if ($operator === null)
            $this->where($column);
        elseif ($value === null)
            $this->where($column,$operator);
        else
            $this->where($column,$operator,$value);

        return $this;
    }

    /**
     * generate list WHERE LIKE parameters with OR boolean operator
     * @param $columns
     * @param $value
     * @return $this
     */
    public function likes($columns,$value)
    {
        foreach ($columns as $item)
        {
            $array = [];
            $array [] = empty($this->where) ? '' : 'or';
            $array [] = $item;
            $array [] = 'like';
            $array [] = '"%'.$value.'%"' ;

            $this->where [] = $array;
        }

        return $this;
    }

    /**
     * generate WHERE LIKE parameter for current query
     * @param $column
     * @param $value
     * @return $this
     */
    public function like($column,$value)
    {
        $array [] = empty($this->where) ? '' : $this->connector;
        $array [] = $column;
        $array [] = 'like';
        $array [] = '"%'.$value.'%"' ;

        $this->where [] = $array;

        $this->connector = 'and';

        return $this;
    }

    /**
     * generate WHERE LIKE parameter for current query with OR
     * @param $column
     * @param $value
     * @return $this
     */
    public function orLike($column,$value)
    {
        $this->connector = 'or';

        $this->like($column,$value);

        return $this;
    }

    /**
     * generate string of ORDER BY for current query
     * @param $column
     * @param string $way
     * @return $this
     */
    public function orderBy($column , $way = 'asc')
    {
        $this->orderBy = " order by $column $way ";

        return $this;
    }

    /**
     * generate string of LIMIT operator for current query
     * @param $limit
     * @param int $start
     * @return $this
     */
    public function limit($limit,$start = 0)
    {
        $this->limit = " limit $start, $limit ";

        return $this;
    }


    /**
     * return a string and binding params for SELECT query
     * @return string
     */
    public function getQuerySelect()
    {
        $this->query = 'select '.((empty($this->select)) ? '*' : implode(' ',$this->select)) . ' from ' .$this->table;

        if (!empty($this->where))
        {
            $this->query .= ' where ';
            foreach ($this->where as $item)
            {
                $this->query .= implode(' ',$item).' ';
            }
        }

        if ($this->orderBy) $this->query .= $this->orderBy;

        if ($this->limit) $this->query .= $this->limit;

        $select = $this->query;

        Log::write($this->query,'query = ');
        Log::write($this->arguments,'args = ');

        $this->query = '';

        $this->arguments = [];

        $this->orderBy = '';

        $this->limit = '';

        return $select;
    }

    /**
     * return string and binding params for update query
     * @param $values
     * @return string
     */
    public function getQueryUpdate($values)
    {
        $this->query = 'update ' . $this->table . ' set ';
        foreach ($values as $key=>$value)
        {
            if (!empty($this->fillable)){
                if (in_array($key,$this->fillable))
                {
                    $this->query .= $key . ' = ?, ';
                    $this->updateArguments [] = $value;
                }
            }else{
                $this->query .= $key . ' = ?, ' ;
                $this->updateArguments [] = $value;
            }
        }

        $this->query .= ' updated_at = now() ';

        if (!empty($this->where))
        {
            $this->query .= ' where ';
            foreach ($this->where as $item)
            {
                $this->query .= implode(' ',$item).' ';
            }
        }

        return $this->query;


    }

    /**
     * return string and binding params for insert query
     * @param $values
     * @return string
     */
    public function getQueryInsert($values)
    {
        $this->query = 'insert into ' . $this->table . ' (' . implode(',' , array_keys($values)) . ',created_at,updated_at) values ( ';

        foreach ($values as $key=>$val)
        {
            if (!empty($this->fillable)){
                if (in_array($key,$this->fillable))
                {
                    $this->query .= ' ?,';
                    $this->arguments [] = $val;
                }
            }else{
                $this->query .= ' ?,';
                $this->arguments [] = $val;
            }

        }

        $this->query .= ' now(), now())';

        return $this->query;
    }

}