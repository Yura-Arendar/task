<?php
namespace Core\Database;

interface QueryInterface
{

    /**
     * Add where param to query
     * @param $column
     * @param null $operator
     * @param null $value
     * @return QueryInterface
     */
    public function where($column, $operator = null, $value = null);

    /**
     * Add where param to query with or boolean operator
     * @param $column
     * @param null $operator
     * @param null $value
     * @return QueryInterface
     */
    public function orWhere($column, $operator = null, $value = null);

    /**
     * Add many likes parameters with or boolean operator
     * @param $columns
     * @param $value
     * @return QueryInterface
     */
    public function likes($columns,$value);

    /**
     * Add like parameter to query
     * @param $column
     * @param $value
     * @return QueryInterface
     */
    public function like($column,$value);

    /**
     * Add like parameter to query with or boolean operator
     * @param $column
     * @param $value
     * @return QueryInterface
     */
    public function orLike($column,$value);

    /**
     * Add order by parameter
     * @param $column
     * @param string $way
     * @return QueryInterface
     */
    public function orderBy($column , $way = 'asc');

    /**
     * Add limit parameter
     * @param $limit
     * @param int $start
     * @return mixed
     */
    public function limit($limit,$start = 0);

    /**
     * Generate string fo query select
     * @return string
     */
    public function getQuerySelect();

    /**
     * Generate string fo query update
     * @return string
     */
    public function getQueryUpdate($values);

    /**
     * Generate string fo query insert
     * @return string
     */
    public function getQueryInsert($values);

}