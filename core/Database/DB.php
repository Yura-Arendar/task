<?php
namespace Core\Database;

use App\Helpers\Log;

class DB
{
    /**
     * PDO object
     * @var \PDO
     */
    private $pdo;

    /**
     * init config and connect to database
     * DB constructor.
     */
    public function __construct()
    {
        $host = config('database.host');
        $database = config('database.dbname');
        $driver = config('database.driver');
        $user = config('database.user');
        $password = config('database.password');
        $charset = config('database.charset');
        $options = config('database.options');

        $connectString = "$driver:host=$host;dbname=$database;charset=$charset";

        try{
            $this->pdo = new \PDO($connectString,$user,$password,$options);
        }catch (\PDOException $e){
            die($e->getMessage());
        }

    }

    /**
     * select query
     * @param $query
     * @param array $parameters
     * @return array
     */
    public function select($query, $parameters = [])
    {
        $execute = $this->pdo->prepare($query);

        $execute->execute($parameters);

        return $execute->fetchAll();

    }

    /**
     * execute query
     * @param $query
     * @param array $parameters
     */
    public function query($query, $parameters = [])
    {
        $execute = $this->pdo->prepare($query);

        $execute->execute($parameters);
    }

    /**
     * return last insert id
     * @return int
     */
    public function lastInsertId()
    {
        return $this->pdo->lastInsertId();
    }
}