<?php
namespace Core;

use App\Models\User;

class Auth
{
    /**
     * return authorized user
     * @return null|array
     */
    public static function getUser()
    {
        if (isset($_SESSION['auth_user']))
            return $_SESSION['auth_user'];
        else
            return null;
    }

    /**
     * login admin user
     * @param $login
     * @param $password
     * @return array|bool|null
     */
    public static function login($login,$password)
    {
        $modelUser = new User();

        $user = $modelUser->where('name',$login)->where('password',md5($password))->first();

        if ($user !== null)
            $_SESSION['auth_user'] = $user;
        else
            return false;

        return $user;
    }

    /**
     * logout admin user
     */
    public static function logout()
    {
        if (!empty($_SESSION['auth_user']))
            unset($_SESSION['auth_user']);
    }

}