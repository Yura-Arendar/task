<?php
namespace Core;
use Core\Routing\Router;

class Route{

    /**
     * object of Core\Routing\Router class
     * @var Router
     */
    protected static $router;

    /**
     * Route constructor.
     */
    private function __construct()
    {

    }


    /**
     * return instance of Router class
     * @return Router
     */
    public static function instance()
    {
        if (is_null(static::$router)) {
            static::$router = new Router();
        }

        return static::$router;
    }


}
