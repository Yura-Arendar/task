<?php

namespace Core\Routing;

use Core\Request;

class Router
{
    /**
     * list of routes
     * @var array
     */
    private $routes = array();

    /**
     * host of current site
     * @var string
     */
    private $host;
    /**
     * object UrlMatcher
     * @var UrlMatcher
     */
    private $mather;

    /**
     * object of UrlGenerator
     * @var UrlGenerator
     */
    private $generator;

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $this->host = Request::get_host();
    }

    /**
     * register a new route
     * @param $name
     * @param $pattern
     * @param $controller
     * @param $action
     * @param string $method
     */
    public function add($name, $pattern, $controller, $action, $method = 'GET')
    {
        $this->routes[$name] = array(
            'pattern' => $pattern,
            'controller' => $controller,
            'action' => $action,
            'method' => $method,
        );
    }

    /**
     * @param $method
     * @param $uri
     * @return MatchedRoute
     */
    public function match($method, $uri)
    {
        return $this->getMatcher()->match($method, $uri);
    }

    /**
     * generate url ny route name
     * @param $name
     * @param array $parameters
     * @param bool $absolute
     * @return string
     */
    public function generate($name, array $parameters = array(), $absolute = false)
    {
        return $this->getGenerator()->generate($name, $parameters, $absolute);
    }

    /**
     * @return UrlMatcher
     */
    private function getMatcher()
    {
        if (null == $this->mather) {
            $this->mather = new UrlMatcher();
            foreach ($this->routes as $route) {
                $this->mather->register($route['method'], $route['pattern'], $route['controller'],$route['action']);
            }
        }

        return $this->mather;
    }

    /**
     * @return UrlGenerator
     */
    private function getGenerator()
    {
        if (null == $this->generator) {
            $this->generator = new UrlGenerator($this->host);
            foreach ($this->routes as $name => $route) {
                $pattern = preg_replace('#\((\w+):(\w+)\)#', '(:$1)', $route['pattern']);
                $this->generator->add($name, $pattern);
            }
        }

        return $this->generator;
    }
}