<?php

namespace Core\Routing;

class MatchedRoute
{
    /**
     * current route controller
     * @var string
     */
    private $controller;

    /**
     * parameters of current route
     * @var array
     */
    private $parameters;

    /**
     * controller action of current route
     * @var string
     */
    private $action;

    /**
     * MatchedRoute constructor.
     * @param $controller
     * @param $action
     * @param array $parameters
     */
    public function __construct($controller, $action, array $parameters = array())
    {
        $this->controller = $controller;
        $this->parameters = $parameters;
        $this->action = $action;
    }

    /**
     * return current controller
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * return parameters of current controller
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * return action of current controller
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }
}