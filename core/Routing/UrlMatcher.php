<?php

namespace Core\Routing;

class UrlMatcher
{
    /**
     * list of methods
     * @var array
     */
    private $methods = array(
        'GET',
        'POST',
        'PUT',
        'DELETE',
        'HEAD'
    );

    /**
     * list of routes grouped by methods
     * @var array
     */
    private $routes = array(
        'GET' => array(),
        'POST' => array(),
        'PUT' => array(),
        'DELETE' => array(),
        'PATCH' => array(),
        'HEAD' => array(),
    );

    /**
     * list of patters route
     * @var array
     */
    private $patterns = array(
        'num' => '[0-9]+',
        'str' => '[a-zA-Z\.\-_%]+',
        'any' => '[a-zA-Z0-9\.\-_%]+',
    );

    /**
     * Add pattern to list
     * @param $name
     * @param $pattern
     */
    public function addPattern($name, $pattern)
    {
        $this->patterns[$name] = $pattern;
    }

    /**
     * register route in routes and list group by method
     * @param $method
     * @param $route
     * @param $controller
     * @param $action
     */
    public function register($method, $route, $controller,$action)
    {
        $methods = strtoupper($method);

        if (false !== strpos($methods, '|'))
        {
            $methods = explode('|', $methods);
        }

        if ($methods == '*')
        {
            $methods = $this->methods;
        }

        $methods = (array)$methods;

        $converted = $this->convertRoute($route);

        foreach ($methods as $m)
        {
            $this->routes[$m][$converted]['controller'] = $controller;
            $this->routes[$m][$converted]['action'] = $action;
        }
    }

    /**
     * @param $route
     * @return mixed
     */
    private function convertRoute($route)
    {
        if (false === strpos($route, '('))
        {
            return $route;
        }

        return preg_replace_callback('#\((\w+):(\w+)\)#', array($this, 'replaceRoute'), $route);
    }

    /**
     *
     * @param $match
     * @return string
     */
    private function replaceRoute($match)
    {
        $name = $match[1];
        $pattern = $match[2];

        return '(?<' . $name . '>' . strtr($pattern, $this->patterns) . ')';
    }

    /**
     * @param $method
     * @param $uri
     * @return MatchedRoute
     */
    public function match($method, $uri)
    {
        $method = strtoupper($method);
        $routes = $this->routes($method);

        if (array_key_exists($uri, $routes)) {
            return new MatchedRoute($routes[$uri]['controller'],$routes[$uri]['action']);
        }

        return $this->doMatch($method, $uri);
    }

    /**
     * return list of routes of current method
     * @param $method
     * @return array|mixed
     */
    private function routes($method)
    {
        return isset($this->routes[$method]) ? $this->routes[$method] : array();
    }

    /**
     * find reoute
     * @param $method
     * @param $uri
     * @return MatchedRoute
     */
    private function doMatch($method, $uri)
    {
        foreach ($this->routes($method) as $route => $value) {
            if (false !== strpos($route, '(')) {
                $pattern = '#^' . $route . '$#s';

                if (preg_match($pattern, $uri, $parameters)) {
                    return new MatchedRoute($value['controller'],$value['action'], $this->processParameters($parameters));
                }
            }
        }
    }

    /**
     * claring parameters
     * @param $parameters
     * @return array
     */
    private function processParameters($parameters)
    {
        foreach ($parameters as $k => $v) {
            if (is_int($k)) {
                unset($parameters[$k]);
            }
        }

        return $parameters;
    }
}