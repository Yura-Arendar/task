<?php
/**
 * main config file
 */

return [

    'view_extension' => '.php',

    'status'=>[
        0 => [
            'class' => 'warning',
            'name' => 'In Work'
        ],
        1 => [
            'class' => 'success',
            'name' => 'Completed'
        ]
    ],

    'table_admin' => [
        'edit'=>[
            'order'=>false,
            'type'=>'edit',
            "searchable" => false,
            'width' => 10,
        ],
        'view'=>[
            'order'=>false,
            'type'=>'view',
            "searchable" => false,
            'width' => 10,
        ],
        'id'=>[
            'order'=>true,
            'type'=>'default',
            "searchable" => false,
            'width'=> 10
        ],

        'completed'=>[
            'order'=>true,
            'type'=>'type',
            "searchable" => false,
            'width'=> 10
        ],

        'name'=>[
            'order'=>true,
            "searchable" => true,
            'type'=>'default',
        ],
        'email'=>[
            'order'=>true,
            "searchable" => true,
            'type'=>'default',
        ],
        'created_at'=>[
            'order'=>true,
            "searchable" => false,
            'type'=>'default',
        ],

    ],

    'table_user' => [
        'view'=>[
            'order'=>false,
            'type'=>'view',
            "searchable" => false,
            'width' => 10,
        ],
        'id'=>[
            'order'=>true,
            'type'=>'default',
            "searchable" => false,
            'width'=> 10
        ],

        'completed'=>[
            'order'=>true,
            'type'=>'type',
            "searchable" => false,
            'width'=> 10
        ],

        'name'=>[
            'order'=>true,
            "searchable" => true,
            'type'=>'default',
        ],
        'email'=>[
            'order'=>true,
            "searchable" => true,
            'type'=>'default',
        ],
        'created_at'=>[
            'order'=>true,
            "searchable" => false,
            'type'=>'default',
        ],

    ]

];