<?php

/**
 * config for database connections
 */
return [

    'driver' => 'mysql',

    'host' => 'localhost',

    'dbname' => 'admin_tasks',

    'user' => 'admin_tasks_user',

    'password' => '5kDqxSrtWX',

    'charset' => 'utf8',

    'options' => [

        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC

    ]

];