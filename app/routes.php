<?php

/**
 * file for register routes
 */


route()->add('index','/',\App\Controllers\IndexController::class,'index');
route()->add('index_ajax','/',\App\Controllers\IndexController::class,'index','POST');
route()->add('login','/login',\App\Controllers\UserController::class,'login');
route()->add('post_login','/postlogin',\App\Controllers\UserController::class,'postLogin','POST');
route()->add('logout','/logout',\App\Controllers\UserController::class,'logout');
route()->add('add_task','/add',\App\Controllers\TaskController::class,'add');
route()->add('submit_task','/postadd',\App\Controllers\TaskController::class,'postAdd','POST');
route()->add('image','/image',\App\Controllers\TaskController::class,'image','POST');
route()->add('preview','/preview',\App\Controllers\TaskController::class,'postPreview','POST');
route()->add('edit','/task/edit/(id:num)',\App\Controllers\TaskController::class,'edit');
route()->add('view','/task/view/(id:num)',\App\Controllers\TaskController::class,'view');
route()->add('update','/task/update/(id:num)',\App\Controllers\TaskController::class,'postUpdate','POST');
