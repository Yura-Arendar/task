<?php
namespace App\Controllers;

class ErrorsController extends BaseController
{

    public $template = 'errors';

    /**
     * render 404 page html
     * @return string
     */
    public function error404()
    {
        header("HTTP/1.0 404 Not Found");

        $this->titleContent = 'Page not found';

        return view()->render('contents/errors/404');
    }

}