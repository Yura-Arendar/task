<?php
namespace App\Controllers;

use Core\Database\QueryInterface;
use Core\Request;

abstract class BaseController
{
    /**
     * name of current layout
     * @var string
     */
    public $template = 'default';

    /**
     * title content
     * @var string
     */
    public $titleContent = '';

    /**
     * if need render views
     * @var bool
     */
    public $render = true;

    /**
     * notification messages
     * @var array
     */
    public $message;

    /**
     * instance of current model for controller
     * @var QueryInterface
     */
    protected $model;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        if (!empty($_SESSION['message'])){
            $this->message = $_SESSION['message'];
            unset($_SESSION['message']);
        }

        if (Request::isAjax())
            $this->render = false;

    }
}