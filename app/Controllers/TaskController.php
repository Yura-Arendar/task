<?php
namespace App\Controllers;

use App\Models\Task;
use Core\Auth;
use Core\Request;
use Intervention\Image\ImageManagerStatic as Image;

class TaskController extends BaseController
{
    /**
     * TaskController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->model = new Task();
    }

    /**
     * Display form for creating a new task
     * @return string
     */
    public function add()
    {
        $this->titleContent = 'Add new task';
        return view()->render('contents/tasks/add');
    }

    /**
     * Create new task
     */
    public function postAdd()
    {
        if ($id = $this->model->insert(Request::postAll()))
        {
            if (Auth::getUser())
                app()->redirect('/task/edit/'.$id,[
                    'type' => 'success',
                    'message' => 'Task saved!'
                ]);
            else
                app()->redirect('/task/view/'.$id,[
                    'type' => 'success',
                    'message' => 'Task saved!'
                ]);
        }


    }

    /**
     * Display form for edit task
     * @param $id
     * @return string
     */
    public function edit($id)
    {
        $task =  $this->model->find($id);

        if (!Auth::getUser())
            app()->redirect('/');

        if ($task === null)
            app()->error404();

        $this->titleContent = 'Edit task: ' . $task['name'];

        return view()->render('contents/tasks/edit',compact('task'));
    }

    /**
     * Update parameters for curent task
     * @param $id
     */
    public function postUpdate($id)
    {
        $data = Request::postAll();

        if (!empty($data['completed']))
            $data['completed'] = 1;
        else
            $data['completed'] = 0;

        $this->model->update($id,$data);

        app()->redirect('/task/edit/'.$id,[
            'type' => 'success',
            'message' => 'Task saved!'
        ]);
    }

    /**
     * Viewing a task
     * @param $id
     * @return string
     */
    public function view($id)
    {
        $task =  $this->model->find($id);


        if ($task === null)
            app()->error404();

        $this->titleContent = 'Edit task: ' . $task['name'];

        return view()->render('contents/tasks/view',compact('task'));
    }

    /**
     * Get html of modal preview
     * @return string
     */
    public function postPreview()
    {
        return view()->render('contents/tasks/preview',Request::postAll());
    }

    /**
     * upload and resize image
     */
    public function image()
    {

        if (!empty($_FILES['file']))
        {
            $newImage = path('/img/common/'.$_FILES['file']['name']);
            if (move_uploaded_file($_FILES['file']['tmp_name'], $newImage)) {

                $image = Image::make($newImage);

                if($image->width()  > 320){
                    $ratio = $image->height()/$image->width();
                    $height = intval(320 * $ratio);
                    $image->resize(320,$height);
                    $image->save($newImage);
                }
                app()->getJson(['image'=>'/img/common/'.$_FILES['file']['name']]);
            } else {
                app()->getJson(['result'=>'error']);
            }
        }
    }
}