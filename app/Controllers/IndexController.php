<?php
namespace App\Controllers;

use App\Helpers\Table;
use App\Models\Task;


class IndexController extends BaseController
{

    /**
     * Show tasks list
     * @return string
     */
    public function index()
    {

        $this->titleContent = 'Tasks list';

        $table = new Table(new Task());

        if ($_POST) app()->getJson($table->getView());

        $table = $table->getView();

        return view()->render('contents/index/content',compact('table'));
    }

}