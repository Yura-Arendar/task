<?php
namespace App\Controllers;

use Core\Auth;
use Core\Request;

class UserController extends BaseController
{
    /**
     * display form to login
     */
    public function login()
    {
        $this->template = 'auth';

        if (Auth::getUser())
            app()->redirect('/');
    }

    /**
     * Authorization of admin user
     */
    public function postLogin()
    {
        $login = Request::_POST('name');
        $password = Request::_POST('password');

        if (! $login || !$password)
            app()->getJson(
                [
                    'type' => 'error',
                    'message' => 'all fields is required!'

                ]
            );

        if (Auth::login($login,$password)){
            app()->redirect('/',[
                'type' => 'success',
                'message' => 'you have successfully logged in!'
            ]);
        }else{
            app()->getJson(
                [
                    'type' => 'error',
                    'message' => 'incorrect username or password!'

                ]
            );
        }
    }

    /**
     * user logout action
     */
    public function logout()
    {
        $this->render = false;
        Auth::logout();
        app()->redirect('/',[
            'type' => 'success',
            'message' => 'good by!'
        ]);
    }
}