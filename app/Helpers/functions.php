<?php

/**
 * return instance of main application class
 * @return \Core\Application
 */
function app()
{
    return \Core\Application::instance();
}

/**
 * return path to application folders
 * @param string $path
 * @return string
 */
function path($path='')
{
    return $_SERVER['DOCUMENT_ROOT'] . $path;
}

/**
 * return value from config file
 * @param $key
 * @param null $default
 * @return array|null|string
 */
function config($key, $default = null)
{
    $key = explode('.',$key);

    $array = include path('/config/'.array_shift($key).'.php');


    foreach ($key as $segment)
    {
        if (isset($array[$segment])){
            $array = $array[$segment];
        }else{
            return $default;
        }
    }


    return $array;
}

/**
 * return instance of route class
 * @return \Core\Route
 */
function route()
{
    return \Core\Route::instance();
}

/**
 * return instance of view class
 * @return \Core\View
 */
function view()
{
    return \Core\View::instance();
}

/**
 * dumping value
 * @param $value
 */
function dd($value)
{
    var_dump($value);
    die();
}
