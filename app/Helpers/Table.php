<?php
namespace App\Helpers;

use Core\Auth;
use Core\Database\QueryInterface;


class Table {

    /**
     * object of model for display data table
     * @var QueryInterface
     */
    public $dataModel;


    /**
     * config for columns
     * @var array|null|string
     */
    public $columns = [];


    /**
     * Table constructor. Binding params
     * @param QueryInterface $oModel
     */
    public function __construct(QueryInterface $oModel)
    {
        $this->dataModel = $oModel;

        $this->columns = Auth::getUser() ? config('app.table_admin') : config('app.table_user');
    }


    /**
     * return json string for ajax request or view to display table
     * @return mixed|string
     */
    public function getView()
    {

        if (!$_POST){
            $data['data'] = [];
            $data['header'] = $this->columns;
            $data = $this->getData();
            return view()->render('table/index',compact('data'));
        }else{
            $data = $this->getData();
            $data['iTotalRecords'] = count($this->dataModel->get());
            $data['iTotalDisplayRecords'] = count($this->dataModel->get());

            $tableData = $_POST;

            $search = [];

            foreach ($tableData['columns'] as $value){
                if ($value['searchable']=='true')
                    $search[] = $value['data'];
            }
            Log::write(count($data['data'] ),'count = ');


            foreach ($data['data'] as $key=>$value){
                foreach ($this->columns as $k=>$v){
                    $data['data'][$key][$k] = view()->render('table/render/'.$v['type'],['aRow'=>$value,'aColumns'=>$data['header'],'key'=>$k]);
                }
            }

            return $data;
        }


    }

    /**
     * return array of table data
     * @return array
     */
    private function getData()
    {
        if ($_POST){
            $tableData = $_POST;

            $search = [];

            foreach ($tableData['columns'] as $value){
                if ($value['searchable']=='true')
                    $search[] = $value['data'];
            }

            $order = $tableData['order'][0];
            if ($tableData['columns'][$order['column']]['orderable']=='true')
                $order['column'] = $tableData['columns'][$order['column']]['data'];
            else{
                $name_col_order = '';
                foreach ($tableData['columns'] as $val){
                    if ($val['orderable']=='true'){
                        $name_col_order = $val['data'];
                        break;
                    }
                }
                $order['column'] = $name_col_order == '' ? 'id' : $name_col_order;
            }

            Log::write($search,$tableData['search']['value'].' = ');
            if ($tableData['search']['value'] && $search)
                $data['data'] = $this->dataModel->likes($search,$tableData['search']['value'])->orderBy($order['column'], $order['dir'])->limit($tableData['length'],$tableData['start'])->get();
            else
                $data['data'] = $this->dataModel->orderBy($order['column'], $order['dir'])->limit($tableData['length'],$tableData['start'])->get();

        }else{

            $data['data'] = [];

        }


        $data['header'] = $this->columns;

        return $data;
    }

}