<?php
namespace App\Helpers;

class Log
{
    const ERROR = 'ERROR: ';
    const NOTICE = 'NOTICE: ';
    const MESSAGE = 'MESSAGE: ';

    /**
     * write data to log file
     * @param $value
     * @param $text
     * @param string $type
     */
    public static function write($value, $text, $type = self::ERROR)
    {
        $file = fopen(path("logs/".date('Y-m-d').".log"),'a+');
        fwrite($file,"\n" . $type . $text . print_r($value,true));
        fclose($file);
    }
}