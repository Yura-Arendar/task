<?php
namespace App\Models;

use Core\Database\BaseModel;

class Task extends BaseModel
{
    protected $table = 'tasks';

    protected $fillable = ['name','email','text','image','text','completed'];
}